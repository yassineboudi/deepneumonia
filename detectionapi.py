import os
import os.path
import torch.utils.data as data
import torch
from PIL import Image
from torchvision import transforms
from torch.autograd import Variable
import json


class DetectionApi():

    def __init__(self, image_path):

        self.device = "cpu"
        self.model_path = "models/pneumonia.pth"
        self.model = torch.load(self.model_path, map_location='cpu')

        self.class_names_path = "config/class_names.json"
        self.image_path = os.path.realpath(image_path)
        self.data_transforms = transforms.Compose([
            transforms.Resize(size=256),
            transforms.CenterCrop(size=224),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [
                0.229, 0.224, 0.225])
        ])
        with open(self.class_names_path) as json_file:
            self.class_names = json.load(json_file)[0]
        self.model.eval()

    def __predict_file(self, image_path):

        img = Image.open(image_path)

        rgbimg = img.convert('RGB')
        image_tensor = self.data_transforms(rgbimg).float()
        image_tensor = image_tensor.unsqueeze_(0)
        input = Variable(image_tensor)
        input = input.to(self.device)
        output = self.model(input)
        index = output.data.cpu().numpy().argmax()

        return self.class_names[index]

    def __predict_dir(self, dir_path):
        results = {}
        for file in os.listdir(dir_path):
            filename = os.fsdecode(file)

            if filename.endswith(".png") or filename.endswith(".jpeg") or filename.endswith(".jpg"):
                file_path = os.path.join(dir_path, filename)
                results[filename] = self.__predict_file(file_path)
        return results

    def predict(self):
        if os.path.isfile(self.image_path):
            return self.__predict_file(self.image_path)
        elif os.path.isdir(self.image_path):
            return self.__predict_dir(self.image_path)
        else:
            return "Files or directory doesn't exist"
