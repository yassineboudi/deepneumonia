# DeePneumonia

A program that can diagnose pneumonia on X-ray images based on deep neural networks.

### Prerequisites

OS tested:

- Ubuntu 16.04

Dependencies:

- PyTorch 1.3.1

## Usage

The program has one mode:

1. Standalone app: a console application.

### Standalone app

```
python standalone.py detect <image_path>
```

#### Inputs:

- `<image_path>`: image path.

#### Output:

- `<output>`: It returns the diagnosis: Normal/Pneumonia.

#### Example:

```
python standalone.py detect test/0001_normal.jpeg
```

## Training:

#### Train phase:

The model was trained for 10 epochs

##### Accuracy:

- train dataset: 0.9684
- validation dataset: 0.9718

##### Loss:

- train dataset: 0.0934
- validation dataset: 0.0981

##### Plot:

#### Test phase:

##### Accuracy:

- **_Normal_**: 98.42%
- **_Pneumonia_**: 97.77%

##### F1 Score:

F1 = 0.9743976562158381

##### Confusion Matrix:

## Test

```
pytest
```

## Dataset

The data used for this app is available at: https://www.kaggle.com/paultimothymooney/chest-xray-pneumonia

## References

https://pytorch.org

https://www.kaggle.com

## Authors

Yassine Boudi
