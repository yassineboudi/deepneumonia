#!usr/bin/python

from detectionapi import DetectionApi
import pytest
import warnings

warnings.filterwarnings("ignore")


def test_detect_normal():
    img_test_path = "test/0001_normal.jpeg"
    detect_disease = DetectionApi(img_test_path)
    assert detect_disease.predict() == {'status': 'normal'}


def test_detect_pneumonia():
    img_test_path = "test/person140_virus_285.jpeg"
    detect_disease = DetectionApi(img_test_path)
    assert detect_disease.predict() == {'status': 'pneumonia'}
